FROM python:3.9.6-alpine3.14
ENV PYTHONUNBUFFERED=1
WORKDIR /rtsd
ADD . /rtsd
COPY ./requirements.txt /rtsd/requirements.txt
RUN pip install -r requirements.txt
COPY . /rtsd
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]