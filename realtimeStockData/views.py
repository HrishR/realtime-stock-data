from django.shortcuts import render
import requests



def home(request):

    return render(request, 'index.html', {'test_var': 'Aashay'})

def displayStock(request, stockName):
    #computations
    #data 5 years etc
    stock_data=requests.get(f"https://jqjfct0r76.execute-api.us-east-1.amazonaws.com/default/PythonPandas?ticker={stockName}&num_days=5").json()
    stock_data_list = stock_data['price_data']
    
    api_key = 'VGK64HGEYBNVW4G0'

    quote_data = requests.get(f"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={stockName}&apikey={api_key}").json()
    quote_data = quote_data['Global Quote']

    general_data = requests.get(f"https://www.alphavantage.co/query?function=OVERVIEW&symbol={stockName}&apikey={api_key}").json()

    trade_advise_data = requests.get(f"https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker={stockName}").json()

    return render(request, 'stock.html', {'stock_name': stockName, 
                                        'stock_data': stock_data,
                                        'stock_data_list': stock_data_list,
                                        'quote_data': quote_data,
                                        'general_data': general_data,
                                        'trade_advise_data': trade_advise_data})
                                       