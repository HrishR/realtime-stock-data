from django.apps import AppConfig


class RealtimestockdataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'realtimeStockData'
