from django.urls import path
from .views import home, displayStock

urlpatterns = [
    path('home/', home, name='home'),
    path('stock/<str:stockName>', displayStock, name='displayStock')
]